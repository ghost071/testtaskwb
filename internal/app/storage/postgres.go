package storage

import (
	model "WBL0/internal/models"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

type PG struct {
	DB *sql.DB
}

var pginstance *PG

func (p *PG) Write(ctx context.Context, data []byte, uid string) error {
	tx, err := p.DB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			if rbErr := tx.Rollback(); rbErr != nil {
				err = fmt.Errorf("write error: %v\nrollback error: %v", err, rbErr)
			}
		}
	}()

	_, err = tx.ExecContext(ctx, "INSERT INTO orders (id, data) VALUES ($1, $2)", uid, data)

	if err != nil {
		return err
	}
	return tx.Commit()
}

func (p *PG) GetAll(ctx context.Context) (m map[string]model.Order, err error) {
	m = make(map[string]model.Order)
	tx, err := p.DB.BeginTx(ctx, nil)
	if err != nil {
		return m, err
	}

	defer func() {
		if err != nil {
			if rbErr := tx.Rollback(); rbErr != nil {
				err = fmt.Errorf("get error: %v\n, rollback error: %v", err, rbErr)
			}
		}
	}()

	rows, err := tx.QueryContext(ctx, "SELECT * FROM orders")
	if err != nil {
		return m, err
	}
	defer rows.Close()

	for rows.Next() {
		var key string
		var value []byte
		err = rows.Scan(&key, &value)
		if err != nil {
			return m, err
		}

		var order model.Order

		err = json.Unmarshal(value, &order)
		if err != nil {
			return m, err
		}
		m[order.OrderUID] = order
	}
	return m, tx.Commit()
}

func NewPg() *PG {
	if pginstance != nil {
		return pginstance
	}

	connStr := fmt.Sprintf("host=%v password=%v user=%v sslmode=%v dbname=%v",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_SSL"),
		os.Getenv("DB_NAME"),
	)

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	pginstance = &PG{DB: db}
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	log.Println("postgres success connected")
	return pginstance
}
