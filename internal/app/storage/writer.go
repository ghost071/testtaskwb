package storage

import (
	"context"
)

type DBWriter interface {
	Write(context.Context, []byte, string) error
}
