package subscriber

import (
	"WBL0/internal/app/storage"
	model "WBL0/internal/models"
	"WBL0/internal/pkg/validator"
	"context"
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/nats-io/stan.go"
)

type SubConf struct {
	Cluster string
	Client  string
	Channel string
}

type Subscriber struct {
	Conf         *SubConf
	Conn         stan.Conn
	Subscription stan.Subscription
}

func (s *Subscriber) Connect() (stan.Conn, error) {
	waitOption := stan.ConnectWait(15 * time.Second)
	connOption := stan.NatsURL(os.Getenv("NATS_ADDR"))
	return stan.Connect(s.Conf.Cluster, s.Conf.Client, connOption, waitOption)
}

func (s *Subscriber) Subscribe(handler stan.MsgHandler,
	option stan.SubscriptionOption) (stan.Subscription, error) {

	return s.Conn.Subscribe(s.Conf.Channel, handler, option)
}

func (s *Subscriber) Unsubscribe() {
	s.Subscription.Unsubscribe()
}

var subinstance *Subscriber

func NewSubscriber() *Subscriber {
	if subinstance != nil {
		return subinstance
	}

	conf := new(SubConf)
	conf.Cluster = os.Getenv("NATS_CLUSTER")
	conf.Client = os.Getenv("NATS_SCLIENT")
	conf.Channel = os.Getenv("NATS_CHAN")

	sub := new(Subscriber)
	sub.Conf = conf

	conn, err := sub.Connect()
	if err != nil {
		log.Fatal(err)
	}

	sub.Conn = conn
	subscription, err := sub.Subscribe(msgHandler,
		stan.DurableName(os.Getenv("NATS_DURABLE")))
	sub.Subscription = subscription
	subinstance = sub
	return subinstance
}

func msgHandler(m *stan.Msg) {
	m.Ack()
	log.Println("Accept new msg")
	err := write(m.Data, storage.NewCache(), storage.NewPg())
	if err != nil {
		log.Println(err)
	}
}

func write(data []byte, destinations ...storage.DBWriter) error {
	ctx := context.TODO()

	order := new(model.Order)
	err := json.Unmarshal(data, order)
	if err != nil {
		return err
	}

	if !validator.IsValid(order) {
		return new(validator.Error)
	}

	for _, db := range destinations {
		err := db.Write(ctx, data, order.OrderUID)
		if err != nil {
			return err
		}
	}
	return nil
}
