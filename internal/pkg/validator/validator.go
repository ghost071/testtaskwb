package validator

import (
	model "WBL0/internal/models"
	"reflect"
	"time"
)

type Error struct{}

func (e *Error) Error() string {
	return "validator: Incorrect data"
}

func IsValid(data *model.Order) bool {
	if len(data.OrderUID) != 19 || data.Items == nil {
		return false
	}

	var t time.Time
	if data.DateCreated == t || len(data.Items) == 0 {
		return false
	}

	flag := true

	for _, item := range data.Items {
		flag = flag && checkFields(item)
	}

	return flag && checkFields(*data, "InternalSignature")
}

func checkFields(data interface{}, exceptions ...string) bool {
	v := reflect.ValueOf(data)

LOOP:
	for i := 0; i < v.NumField(); i++ {
		val := v.Field(i)
		fname := v.Type().Field(i).Name

		for _, ex := range exceptions {
			if fname == ex {
				continue LOOP
			}
		}

		switch {
		case fname == "Delivery":
			if order, ok := data.(model.Order); ok {
				if !checkFields(order.Delivery) {
					return false
				}
			}
		case fname == "Payment":
			if order, ok := data.(model.Order); ok {
				if !checkFields(order.Payment, "RequestID") {
					return false
				}
			}
		case val.Type().String() != "string":
			continue
		default:
			if len(val.String()) == 0 {
				return false
			}
		}
	}
	return true
}
