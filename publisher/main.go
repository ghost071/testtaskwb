package main

import (
	model "WBL0/internal/models"
	"encoding/json"
	"io"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/nats-io/stan.go"
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	testData := new(model.Order)

	f, err := os.Open("publisher/model.json")
	if err != nil {
		log.Fatal(err)
	}

	b, err := io.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}

	if err = json.Unmarshal(b, testData); err != nil {
		log.Fatal(err)
	}

	sc, err := stan.Connect(os.Getenv("NATS_CLUSTER"), "prod1")
	if err != nil {
		log.Fatal(err)
	}

	for {
		pbBytes, err := json.Marshal(testData)
		if err != nil {
			log.Fatal(err)
		}

		sc.Publish(os.Getenv("NATS_CHAN"), pbBytes)
		log.Println("Sent message with uid:", testData.OrderUID)

		newUID := generate(19)
		testData.OrderUID = newUID
		time.Sleep(5 * time.Second)
	}
}

func generate(n int) (s string) {
	symbs := "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		s += string(symbs[rand.Intn(len(symbs))])
	}
	return
}
