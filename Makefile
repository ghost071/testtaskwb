start:
	docker-compose up
up:
	docker run -v ~/desktop/wb/wb-l0/migrations:/migrations --network host migrate/migrate -path migrations -database "postgres://postgres:1132@localhost:5433/postgres?sslmode=disable" up
down:   
	docker run -v ~/desktop/wb/wb-l0/migrations:/migrations --network host migrate/migrate -path migrations -database "postgres://postgres:1132@localhost:5433/postgres?sslmode=disable" down -all 

stop:
	docker-compose stop
