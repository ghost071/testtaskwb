package main

import (
	"WBL0/internal/app/server"
	"WBL0/internal/app/subscriber"
	"context"
	"log"

	"github.com/joho/godotenv"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	ctx := context.Background()
	go subscriber.NewSubscriber()

	srv := server.New()
	if err := server.Start(ctx, srv); err != nil {
		log.Fatal(err)
	}
}
