FROM golang:1.18

ENV POSTGRES_PASSWORD=1132

RUN mkdir ../wbl0
COPY . ../wbl0
WORKDIR ../wbl0

RUN apt-get update
RUN apt-get -y install postgresql-client
RUN chmod +x script.sh
RUN go build -o main.go ./cmd

EXPOSE 8080
